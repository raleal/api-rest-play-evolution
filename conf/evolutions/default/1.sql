# --- First database schema

# --- !Ups

--
-- Base de datos: db_play
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla cliente
--

CREATE TABLE cliente (
  id int(11) NOT NULL,
  dni varchar(255) NOT NULL,
  nombre varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  apellido varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  telefono varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  email varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla cliente
--
ALTER TABLE cliente
  ADD PRIMARY KEY (id);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla cliente
--
ALTER TABLE cliente
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

# --- !Downs

drop table if exists cliente;

