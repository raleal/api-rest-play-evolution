# --- Sample dataset

# --- !Ups

INSERT INTO cliente (dni, nombre, apellido, telefono, email) VALUES ('17857523', 'Jhon', 'Doe', '+4385741236', 'jdoe@email.com'); 
INSERT INTO cliente (dni, nombre, apellido, telefono, email) VALUES ('19258963', 'Reynolds', 'Smith', '+548596321', 'rsmith@gmail.com');

# --- !Downs

delete from cliente;