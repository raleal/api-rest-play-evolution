package controllers;

import models.Cliente;
import com.fasterxml.jackson.databind.JsonNode;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import repository.ClienteRepository;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.util.Map;
import java.lang.Iterable;
import java.util.concurrent.CompletionStage;
import play.mvc.Http;
import java.util.Optional;
import java.util.HashMap;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ClienteController extends Controller {

    private final ClienteRepository clienteRepository;
    private final HttpExecutionContext httpExecutionContext;
    private final FormFactory formFactory;

    @Inject
    public ClienteController(FormFactory formFactory,
                            ClienteRepository clienteRepository,
                          HttpExecutionContext httpExecutionContext) {
        this.clienteRepository = clienteRepository;  
        this.httpExecutionContext = httpExecutionContext;
        this.formFactory = formFactory;
    }
    
    public CompletionStage<Result> list() {

        return clienteRepository.json().thenApplyAsync(list -> {
            JsonNode clienteJson = Json.toJson(list);
            return ok(clienteJson);
        }, httpExecutionContext.current());

    }

    public CompletionStage<Result> findId(Long id) {
        
        return clienteRepository.findId(id).thenApplyAsync(cliente -> {
            JsonNode clienteJson = Json.toJson(cliente);
            return ok(clienteJson);
        }, httpExecutionContext.current());

    }

    public CompletionStage<Result> findAll(String dni, String nombre, String apellido, String telefono, String email) {

        JsonNode json = JsonNodeFactory.instance.objectNode();
        
        if(dni!=null){

            return clienteRepository.filter("dni", dni).thenApplyAsync(list -> {
            JsonNode clienteJson = Json.toJson(list);
            return ok(clienteJson);
            }, httpExecutionContext.current());

        }else if(nombre!=null){

            return clienteRepository.filter("nombre", nombre).thenApplyAsync(list -> {
            JsonNode clienteJson = Json.toJson(list);
            return ok(clienteJson);
            }, httpExecutionContext.current());

        }else if(apellido!=null){

            return clienteRepository.filter("apellido", apellido).thenApplyAsync(list -> {
            JsonNode clienteJson = Json.toJson(list);
            return ok(clienteJson);
            }, httpExecutionContext.current());

        }else if(telefono!=null){

            return clienteRepository.filter("telefono", telefono).thenApplyAsync(list -> {
            JsonNode clienteJson = Json.toJson(list);
            return ok(clienteJson);
            }, httpExecutionContext.current());

        }else if(email!=null){

            return clienteRepository.filter("email", email).thenApplyAsync(list -> {
            JsonNode clienteJson = Json.toJson(list);
            return ok(clienteJson);
            }, httpExecutionContext.current());

        }else{
                
            return clienteRepository.validate().thenApplyAsync(v -> {
            JsonNode clienteJson = Json.toJson(v);
            return badRequest(clienteJson);
            }, httpExecutionContext.current());

            }

    }

    public CompletionStage<Result> insert() {

        Form<Cliente> clienteForm = formFactory.form(Cliente.class).bindFromRequest();
        Cliente cliente = clienteForm.get();

        return clienteRepository.insert(cliente).thenApplyAsync(data -> {
            
            JsonNode clienteJson = Json.toJson(cliente);
            return ok(clienteJson);
        }, httpExecutionContext.current());

    }

    public CompletionStage<Result> update() throws PersistenceException {

        Form<Cliente> clienteForm = formFactory.form(Cliente.class).bindFromRequest();
        Cliente cliente = clienteForm.get();

        return clienteRepository.update(cliente).thenApplyAsync(data -> {
                JsonNode clienteJson = Json.toJson(cliente);
                return ok(clienteJson);
            }, httpExecutionContext.current());

    }

    public CompletionStage<Result> delete(Long id) {

        JsonNode json = JsonNodeFactory.instance.objectNode();
        
        return clienteRepository.delete(id).thenApplyAsync(v -> {
            if(!v.equals(Optional.empty())){

                ((ObjectNode)json).put("Exito", "Borrado con Exito");
                return ok(json);

            }else{
                
                ((ObjectNode)json).put("Error", "Error Eliminando el id "+id);
                return badRequest(json);

            }
            
            
        }, httpExecutionContext.current());
    }

}
            
