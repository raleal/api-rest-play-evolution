package models;

import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Cliente entity managed by Ebean
 */
@Entity 
public class Cliente extends BaseModel {

    private static final long serialVersionUID = 1L;

    @Constraints.Required
    public String dni;

    @Constraints.Required
    public String nombre;
    
    @Constraints.Required
    public String apellido;

    @Constraints.Required
    public String telefono;

    @Constraints.Required
    public String email;

    public Cliente() {
    }

    public Cliente(String dni, String nombre, String apellido, String telefono, String email) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.email = email;
    }

    
}

