package repository;

import io.ebean.*;
import models.Cliente;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http;
import play.libs.Json;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class ClienteRepository {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public ClienteRepository(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }

    public CompletionStage<List<Cliente>> json() {
        return supplyAsync(() ->
                ebeanServer.find(Cliente.class).findList(), executionContext);
    }

    public CompletionStage<Optional<Cliente>> findId(Long id) {
        return supplyAsync(() -> Optional.ofNullable(ebeanServer.find(Cliente.class).setId(id).findOne()), executionContext);
    }

    public CompletionStage<List<Cliente>> filter(String field, String filter) {
        return supplyAsync(() ->
                ebeanServer.find(Cliente.class).where()
                    .ilike(field, "%" + filter + "%")
                    .findList(), executionContext);
    }

    public CompletionStage<Cliente> insert(Cliente cliente) {

        return supplyAsync(() -> {
             ebeanServer.insert(cliente);
             return cliente;
        }, executionContext);
                
    }

    public CompletionStage<Cliente> update(Cliente cliente) {
        return supplyAsync(() -> {
            Transaction txn = ebeanServer.beginTransaction();
            try {
                Cliente clienteBD = ebeanServer.find(Cliente.class).setId(cliente.id).findOne();
                if (clienteBD != null) {
                    clienteBD.dni = cliente.dni;
                    clienteBD.nombre = cliente.nombre;
                    clienteBD.apellido = cliente.apellido;
                    clienteBD.telefono = cliente.telefono;
                    clienteBD.email = cliente.email;

                    clienteBD.update();
                    txn.commit();
                }
            } finally {
                txn.end();
            }
            return cliente;
        }, executionContext);
    }

    public CompletionStage<Optional<Long>> delete(Long id) {
        return supplyAsync(() -> {
            try {
                final Optional<Cliente> cliente = Optional.ofNullable(ebeanServer.find(Cliente.class).setId(id).findOne());
                cliente.ifPresent(Model::delete);
                return cliente.map(c -> c.id);
            } catch (Exception e) {
                return Optional.empty();
            }
        }, executionContext);
    }

    public CompletionStage<JsonNode> validate() {

        JsonNode json = JsonNodeFactory.instance.objectNode();
        ((ObjectNode)json).put("Error", "Revisar los datos enviados");
        return supplyAsync(() ->
                json, executionContext);
    }

}
